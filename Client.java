package org.example;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws FileNotFoundException {
        try {
            Socket socket = new Socket("127.0.0.1", 12345);

            File file = new File("/Users/arberxhauri/Desktop/file.txt");
            byte[] fileData = new byte[(int) file.length()];
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(fileData);

            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(fileData, 0 , fileData.length);
            outputStream.flush();

            fileInputStream.close();
            outputStream.close();
            socket.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
