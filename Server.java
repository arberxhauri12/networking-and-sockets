package org.example;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        try{
            ServerSocket serverSocket = new ServerSocket(12345);

            while (true){
                Socket clientSocket = serverSocket.accept();

                InputStream inputStream = clientSocket.getInputStream();

                File receivedFile = new File("/Users/arberxhauri/Documents/file.txt");
                FileOutputStream fileOutputStream = new FileOutputStream(receivedFile);
                byte[] buffer = new byte[1024];

                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1){
                    fileOutputStream.write(buffer, 0 , bytesRead);
                }

                fileOutputStream.close();
                inputStream.close();
                clientSocket.close();
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
